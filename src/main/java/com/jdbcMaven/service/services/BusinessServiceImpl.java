package com.jdbcMaven.service.services;

import com.jdbcMaven.service.Dao.JdbcOperationService;

public class BusinessServiceImpl {
	private final JdbcOperationService jdbcOperationService;

	// Normal statement constructor
	public BusinessServiceImpl(JdbcOperationService jdbcOperationService) {
		this.jdbcOperationService = jdbcOperationService;
	}

	// Normal select service
	public void selectService() {
		jdbcOperationService.doSelect();
	}

	// Normal insert service
	public void insertService() {
		jdbcOperationService.doInsert();
	}

	// Normal update service
	public void updateService() {
		jdbcOperationService.doUpdate();
	}

	// Normal delete service
	public void deletService() {
		jdbcOperationService.doDelete();
	}
}

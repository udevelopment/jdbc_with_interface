package com.jdbcMaven.service.services;

import java.util.List;

import com.jdbc.maven.model.UserDetail;
import com.jdbcMaven.service.Dao.JdbcListOperationService;

public class BusinessListServiceImpl {
	private final JdbcListOperationService<UserDetail> jdbcListOperationService;

	// Normal statement constructor
	public BusinessListServiceImpl(JdbcListOperationService<UserDetail> jdbcListOperationService) {
		this.jdbcListOperationService = jdbcListOperationService;
	}

	// Normal select service
	public void selectService() {
		jdbcListOperationService.doSelect();
	}

	// Normal insert service
	public void insertService(List<UserDetail> list) {
		jdbcListOperationService.doInsert(list);
	}

	// Normal update service
	public void updateService(UserDetail listUser) {
		jdbcListOperationService.doUpdate(listUser);
	}

	// Normal delete service
	public void deletService(UserDetail listUser) {
		jdbcListOperationService.doDelete(listUser);
	}
}

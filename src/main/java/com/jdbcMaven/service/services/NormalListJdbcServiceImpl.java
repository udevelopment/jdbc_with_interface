package com.jdbcMaven.service.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.jdbc.maven.model.UserDetail;
import com.jdbcMaven.JDBCinterface.JdbcConnection;
import com.jdbcMaven.service.Dao.JdbcListOperationService;

public class NormalListJdbcServiceImpl implements JdbcListOperationService<UserDetail> {

	public List<UserDetail> doSelect() {
		// TODO Auto-generated method stub
		// JdbcConnection jdbcConnection = new JdbcConnection();
		// Connection conn = null;
		// PreparedStatement stmt = null;
		// ResultSet rs = null;
		// String query = "select*from sampledatabase.signup where idsignup=?";
		// System.out.print("\n[Performing SELECT] ... ");
		//
		// try {
		// conn = jdbcConnection.getConnection();
		// stmt = conn.prepareStatement(query);
		// stmt.setString(1, "1");
		// rs = stmt.executeQuery();
		// while (rs.next()) {
		// String username = rs.getString("username");
		// String email = rs.getString("email");
		// System.out.println(username + " " + email);
		// }
		// } catch (SQLException e) {
		// System.out.println(e.getMessage());
		// throw new RuntimeException(e);
		// } finally {
		// jdbcConnection.closeConnection(conn, stmt, rs);
		// }
		return null;
	}

	public void doInsert(List<UserDetail> list) {
		// TODO Auto-generated method stub
		JdbcConnection jdbcConnection = new JdbcConnection();
		Connection conn = null;
		PreparedStatement stmt = null;
		String query = "INSERT INTO sampledatabase.signup(idsignup,email,username,password,phone,salary) "
				+ "VALUES (?,?,?,?,?,?)";
		// TODO Auto-generated method stub
		System.out.print("\n[Performing INSERT] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.prepareStatement(query);
			for (UserDetail user : list) {
				stmt.setString(1, user.getUserId()); // 1 is the first ? (1
				// based counting)
				stmt.setString(2, user.getUserEmail());
				stmt.setString(3, user.getUserName());
				stmt.setString(4, user.getUserpassword());
				stmt.setString(5, user.getUserPhone());
				stmt.setString(6, user.getUserSalary());
				stmt.addBatch();
			}
			stmt.executeBatch();
			System.out.println("Successfully Pre Inserted");
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			throw new RuntimeException(ex);
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
	}

	public UserDetail doUpdate(UserDetail list) {
		// TODO Auto-generated method stub
		JdbcConnection jdbcConnection = new JdbcConnection();
		Connection conn = null;
		PreparedStatement stmt = null;
		String query = "UPDATE sampledatabase.signup SET username=? WHERE idsignup=?";
		// TODO Auto-generated method stub
		System.out.print("\n[Performing UPDATE] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.prepareStatement(query);
			stmt.setString(1, list.getUserName());
			stmt.setString(2, list.getUserId());
			stmt.addBatch();
			stmt.executeBatch();
			System.out.println("Successfully Pre Updated");
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
			throw new RuntimeException(ex);
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
		return null;
	}

	public void doDelete(UserDetail list) {
		// TODO Auto-generated method stub
		JdbcConnection jdbcConnection = new JdbcConnection();
		Connection conn = null;
		PreparedStatement stmt = null;
		String query = "DELETE FROM sampledatabase.signup WHERE idsignup=?";
		// TODO Auto-generated method stub
		System.out.print("\n[Performing DELETE] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.prepareStatement(query);
			stmt.setString(1, "5");
			stmt.addBatch();
			stmt.executeBatch();
			System.out.println("Successfully Pre Deleted");
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
			throw new RuntimeException(ex);
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
	}

}

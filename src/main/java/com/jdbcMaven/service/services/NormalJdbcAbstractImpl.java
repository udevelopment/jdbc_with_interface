package com.jdbcMaven.service.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.jdbcMaven.JDBCinterface.JdbcConnection;
import com.jdbcMaven.service.Dao.JdbcOperationAbstrctService;

public class NormalJdbcAbstractImpl extends JdbcOperationAbstrctService {

	@Override
	public void doSelect() {
		// TODO Auto-generated method stub
		JdbcConnection jdbcConnection = new JdbcConnection();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "select*from sampledatabase.signup";
		System.out.print("\n[Performing SELECT] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				String username = rs.getString("username");
				String email = rs.getString("email");
				System.out.println(username + "   " + email);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(e);
		} finally {
			jdbcConnection.closeConnection(conn, stmt, rs);
		}
	}

	@Override
	public void doInsert() {
		// TODO Auto-generated method stub
		JdbcConnection jdbcConnection = new JdbcConnection();
		Connection conn = null;
		Statement stmt = null;
		// TODO Auto-generated method stub
		System.out.print("\n[Performing INSERT] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate("INSERT INTO sampledatabase.signup "
					+ "VALUES (3,'vin@gmail.com', 'VInnie', 'Test@123', 6565565656,500)");
			System.out.println("Successfully Inserted");
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			throw new RuntimeException(ex);
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
	}

	@Override
	public void doUpdate() {
		// TODO Auto-generated method stub
		JdbcConnection jdbcConnection = new JdbcConnection();
		Connection conn = null;
		Statement stmt = null;
		// TODO Auto-generated method stub
		System.out.print("\n[Performing UPDATE] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate("UPDATE sampledatabase.signup SET username='paul walker' WHERE idsignup='2'");
			System.out.println("Successfully Updated");
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
			throw new RuntimeException(ex);
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
	}

	@Override
	public void doDelete() {
		// TODO Auto-generated method stub
		JdbcConnection jdbcConnection = new JdbcConnection();
		Connection conn = null;
		Statement stmt = null;
		// TODO Auto-generated method stub
		System.out.print("\n[Performing DELETE] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate("DELETE FROM sampledatabase.signup WHERE idsignup='3'");
			System.out.println("Successfully Deleted");
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
			throw new RuntimeException(ex);
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
	}

}

package com.jdbcMaven.service.Dao;

public abstract class JdbcOperationAbstrctService {

	// abstract method
	public abstract void doSelect();

	public abstract void doInsert();

	// public abstract void doUpdate();
	//
	// public abstract void doDelete();

	// public void doSelect() {
	// }
	//
	// public void doInsert() {
	// }

	// normal method
	public void doUpdate() {
	}

	public void doDelete() {
	}
}

package com.jdbcMaven.JDBCinterface;

import java.util.ArrayList;
import java.util.List;

import com.jdbc.maven.model.UserDetail;
import com.jdbcMaven.service.Dao.JdbcListOperationService;
import com.jdbcMaven.service.services.BusinessListServiceImpl;
import com.jdbcMaven.service.services.NormalListJdbcServiceImpl;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Change this implementation according your requirements
		// JdbcOperationService dao = new NormalJdbcServiceImpl();

		// JdbcOperationService dao = new PreparedJdbcServiceImpl();

		// JdbcOperationAbstrctService dao = new NormalJdbcAbstractImpl();
		JdbcListOperationService<UserDetail> dao = new NormalListJdbcServiceImpl();
		// BusinessServiceImpl service = new BusinessServiceImpl(dao);

		// BusinessAbstractServiceImpl service = new
		// BusinessAbstractServiceImpl(dao);
		List<UserDetail> userList = new ArrayList<UserDetail>();
		UserDetail user = new UserDetail();
		user.setUserId("4");
		user.setUserEmail("rock@gmail.com");
		user.setUserName("Rock");
		user.setUserpassword("Test@123");
		user.setUserPhone("9845763215");
		user.setUserSalary("45820");
		userList.add(user);

		UserDetail user1 = new UserDetail();
		user1.setUserId("5");
		user1.setUserEmail("jacky@gmail.com");
		user1.setUserName("Jacks");
		user1.setUserpassword("Test@123");
		user1.setUserPhone("9845763215");
		user1.setUserSalary("45820");
		userList.add(user1);

		UserDetail user3 = new UserDetail();
		user3.setUserId("4");
		user3.setUserName("Jacksddd");

		UserDetail user4 = new UserDetail();
		user4.setUserId("5");

		BusinessListServiceImpl service = new BusinessListServiceImpl(dao);

		service.selectService();
		service.insertService(userList);
		service.updateService(user3);
		service.deletService(user4);
	}

}

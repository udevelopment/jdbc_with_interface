package com.jdbcMaven.service.services;

import com.jdbcMaven.service.Dao.JdbcOperationAbstrctService;

public class BusinessAbstractServiceImpl {
	private final JdbcOperationAbstrctService jdbcOperationAbstrctService;

	// Normal statement constructor
	public BusinessAbstractServiceImpl(JdbcOperationAbstrctService jdbcOperationAbstrctService) {
		this.jdbcOperationAbstrctService = jdbcOperationAbstrctService;
	}

	// Normal select service
	public void selectService() {
		jdbcOperationAbstrctService.doSelect();
	}

	// Normal insert service
	public void insertService() {
		jdbcOperationAbstrctService.doInsert();
	}

	// Normal update service
	public void updateService() {
		jdbcOperationAbstrctService.doUpdate();
	}

	// Normal delete service
	public void deletService() {
		jdbcOperationAbstrctService.doDelete();
	}
}

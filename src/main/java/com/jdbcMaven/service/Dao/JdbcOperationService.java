package com.jdbcMaven.service.Dao;

public interface JdbcOperationService {

	void doSelect();

	void doInsert();

	void doUpdate();

	void doDelete();

}

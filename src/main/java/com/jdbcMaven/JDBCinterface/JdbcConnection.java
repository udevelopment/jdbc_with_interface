package com.jdbcMaven.JDBCinterface;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcConnection {

	public Connection getConnection() {
		// TODO Auto-generated method stub
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sampledatabase", "root", "");
		} catch (ClassNotFoundException ex) {
			System.out.println(ex.getMessage());
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return conn;
	}

	public void closeConnection(Connection conn, Statement stmt, ResultSet rs) {
		try {
			if (conn != null) {
				conn.close();
			}
			System.out.println("CLOSE CONNECTION---" + conn);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if (stmt != null) {
				stmt.close();
			}
			System.out.println("CLOSE STATEMENT---" + stmt);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if (rs != null) {
				rs.close();
			}
			System.out.println("CLOSE RESULTSET---" + rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package com.jdbcMaven.service.Dao;

import java.util.List;

public interface JdbcListOperationService<T> {

	List<T> doSelect();

	void doInsert(List<T> list);

	T doUpdate(T list);

	void doDelete(T list);
}

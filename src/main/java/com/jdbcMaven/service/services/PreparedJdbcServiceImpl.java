package com.jdbcMaven.service.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.jdbcMaven.JDBCinterface.JdbcConnection;
import com.jdbcMaven.service.Dao.JdbcOperationService;

public class PreparedJdbcServiceImpl implements JdbcOperationService {

	public void doSelect() {
		// TODO Auto-generated method stub
		JdbcConnection jdbcConnection = new JdbcConnection();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query = "select*from sampledatabase.signup where idsignup=?";
		System.out.print("\n[Performing SELECT] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.prepareStatement(query);
			stmt.setString(1, "1");
			rs = stmt.executeQuery();
			while (rs.next()) {
				String username = rs.getString("username");
				String email = rs.getString("email");
				System.out.println(username + "   " + email);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(e);
		} finally {
			jdbcConnection.closeConnection(conn, stmt, rs);
		}
	}

	public void doInsert() {
		// TODO Auto-generated method stub
		JdbcConnection jdbcConnection = new JdbcConnection();
		Connection conn = null;
		PreparedStatement stmt = null;
		String query = "INSERT INTO sampledatabase.signup(idsignup,email,username,password,phone,salary) "
				+ "VALUES (?,?,?,?,?,?)";
		// TODO Auto-generated method stub
		System.out.print("\n[Performing INSERT] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.prepareStatement(query);
			stmt.setString(1, "3");
			stmt.setString(2, "vin@gmail.com");
			stmt.setString(3, "Vinnie");
			stmt.setString(4, "Test@123");
			stmt.setString(5, "9635821456");
			stmt.setString(6, "35000");
			stmt.executeUpdate();
			System.out.println("Successfully Pre Inserted");
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			throw new RuntimeException(ex);
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
	}

	public void doUpdate() {
		// TODO Auto-generated method stub
		JdbcConnection jdbcConnection = new JdbcConnection();
		Connection conn = null;
		PreparedStatement stmt = null;
		String query = "UPDATE sampledatabase.signup SET username=? WHERE idsignup=?";
		// TODO Auto-generated method stub
		System.out.print("\n[Performing UPDATE] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.prepareStatement(query);
			stmt.setString(1, "paul walker");
			stmt.setString(2, "2");
			stmt.executeUpdate();
			System.out.println("Successfully Pre Updated");
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
			throw new RuntimeException(ex);
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
	}

	public void doDelete() {
		// TODO Auto-generated method stub
		JdbcConnection jdbcConnection = new JdbcConnection();
		Connection conn = null;
		PreparedStatement stmt = null;
		String query = "DELETE FROM sampledatabase.signup WHERE idsignup=?";
		// TODO Auto-generated method stub
		System.out.print("\n[Performing DELETE] ... ");
		try {
			conn = jdbcConnection.getConnection();
			stmt = conn.prepareStatement(query);
			stmt.setString(1, "3");
			stmt.executeUpdate();
			System.out.println("Successfully Pre Deleted");
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
			throw new RuntimeException(ex);
		} finally {
			jdbcConnection.closeConnection(conn, stmt, null);
		}
	}

}
